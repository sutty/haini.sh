# Haini.sh

Genera un entorno de trabajo reproducible para Sutty.

Mirá la [guía para configurar el entorno de trabajo](https://0xacab.org/sutty/docs.sutty.nl/-/blob/no-masters/desarrollo/configurar-entorno.md)
para usarlo.

## Contexto

Instalar gemas toma mucho tiempo y las distribuciones que usan `apt` es
necesario instalar librerías que no se llaman como deberían llamarse.

Por otro lado, `bundler` está desarrollando soporte para gemas binarias
de otras librerías de C, como musl, pero el soporte aun está incompleto
y es muy posible que termines instalando gemas de musl en glibc,
rompiendo todo el entorno.

Aparte, necesitamos algunas caracteristicas en el entorno de desarrollo
como HTTPS que se pueden automatizar para ahorrar dolores de cabeza.
Por eso, haini.sh también genera certificados HTTPS para usarse
localmente con dominios `*.sutty.local`.

## Dependencias

- busybox
- Bubblewrap (bwrap)

## Licencia

Haini.sh se encuentra bajo los términos de la licencia MIT Antifascista.
